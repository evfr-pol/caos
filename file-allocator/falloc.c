#include "falloc.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

void falloc_init(file_allocator_t* allocator, const char* filepath,
                 uint64_t allowed_page_count) {
  struct stat my_stat;
  int result = stat(filepath, &my_stat);
  if (result == -1) {
    if (errno != ENOENT) {
      perror("Problems with pathname");
      exit(1);
    }

    if (mknod(filepath, S_IRWXU | S_IRWXG | S_IRWXO | S_IFREG, 0) == -1) {
      perror("Cannot create file");
      exit(1);
    }

    int fd = open(filepath, O_RDWR);
    if (fd == -1) {
      perror("Cannot open file");
      exit(1);
    }
    allocator->fd = fd;

    int err = ftruncate(fd, PAGE_MASK_SIZE * sizeof(uint64_t) +
                                allowed_page_count * PAGE_SIZE + 1);
    if (err == -1) {
      perror("Cannot fit the file");
      return;
    }

    void* ptr =
        mmap(NULL,
             PAGE_MASK_SIZE * sizeof(uint64_t) + allowed_page_count * PAGE_SIZE,
             PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (ptr == MAP_FAILED) {
      perror("Cannot map the fileqqqqq");
      exit(1);
    }

    allocator->page_mask = (uint64_t*)(ptr + allowed_page_count * PAGE_SIZE);
    for (int i = 0; i < PAGE_MASK_SIZE; ++i) {
      *(allocator->page_mask + i) = 0;
    }
    allocator->curr_page_count = 0;
    allocator->allowed_page_count = allowed_page_count;
    allocator->base_addr = ptr;
    return;
  }
  int fd = open(filepath, O_RDWR);
  if (fd == -1) {
    perror("Cannot open the file");
    exit(2);
  }
  allocator->fd = fd;
  void* ptr = mmap(
      NULL, PAGE_MASK_SIZE * sizeof(uint64_t) + allowed_page_count * PAGE_SIZE,
      PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

  if (ptr == MAP_FAILED) {
    perror("Cannot map the file");
    exit(1);
  }
  allocator->page_mask = (uint64_t*)(ptr + allowed_page_count * PAGE_SIZE);
  allocator->allowed_page_count = allowed_page_count;
  allocator->base_addr = ptr;
  allocator->curr_page_count = 0;
  for (int i = 0; i < PAGE_MASK_SIZE; ++i) {
    allocator->curr_page_count +=
        __builtin_popcountll(*(allocator->page_mask + i));
  }
}

void falloc_destroy(file_allocator_t* allocator) {
  if (munmap(allocator->base_addr,
             PAGE_MASK_SIZE * sizeof(uint64_t) +
                 allocator->allowed_page_count * PAGE_SIZE) == -1) {
    perror("Cannot munmap");
    exit(1);
  }
  allocator->base_addr = NULL;
  allocator->page_mask = NULL;
  if (close(allocator->fd) != 0) {
    perror("Cannot close");
    exit(1);
  }
}

void* falloc_acquire_page(file_allocator_t* allocator) {
  if (allocator->curr_page_count == allocator->allowed_page_count) {
    return NULL;
  }
  for (uint64_t i = 0; i < allocator->allowed_page_count; ++i) {
    if ((*(allocator->page_mask + (i / 8ULL)) & (1 << (i % 8ULL))) == 0) {
      (*(allocator->page_mask + (i / 8ULL))) += (1 << (i % 8ULL));
      ++allocator->curr_page_count;
      return allocator->base_addr + i * PAGE_SIZE;
    }
  }
}

void falloc_release_page(file_allocator_t* allocator, void** addr) {
  if (allocator->curr_page_count == 0) {
    perror("Cannot release resources");
    return;
  }
  uint64_t pos = (*addr - allocator->base_addr) / PAGE_SIZE;
  (*(allocator->page_mask + (pos / 8ULL))) -= (1 << (pos % 8ULL));
  --allocator->curr_page_count;
  *addr = NULL;
}
