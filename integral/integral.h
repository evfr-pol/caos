#pragma once

#include <math.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "wait.h"

typedef double field_t;

typedef field_t func_t(field_t);

typedef struct part_int {
  double left_bound;
  double right_bound;
  func_t* func;
  double step;
  double result;
  _Atomic(uint32_t)* ptr;
  _Atomic(uint32_t) old;
  _Atomic(uint32_t) need_to_exit;
  _Atomic(uint32_t) is_run;
} part_int_t;

void* integrate(void* part) {
  part_int_t* object = (part_int_t*)part;
  _Atomic(uint32_t) value = 1;
  while (true) {
    object->is_run = 0;
    while (true) {
      atomic_wait(object->ptr, object->old);
      if (!atomic_compare_exchange_strong(object->ptr, &object->old,
                                          object->old)) {
        break;
      }
    }
    if (atomic_compare_exchange_strong(&object->need_to_exit, &value, 1)) {
      pthread_exit(part);
    }
    value = 1;
    object->result = 0;
    double i = object->left_bound;
    for (; i < object->right_bound - object->step; i += object->step) {
      object->result +=
          ((object->func(i) + object->func(i + object->step)) / 2.0l) *
          (object->step);
    }
    object->result +=
        ((object->func(i) + object->func(object->right_bound)) / 2.0l) *
        (object->right_bound - i);
  }
  return NULL;
}

typedef struct par_integrator {
  part_int_t* parts;
  pthread_t* threads;
  size_t threads_num;
  double step;
  _Atomic(uint32_t) atomic;
} par_integrator_t;

int par_integrator_init(par_integrator_t* self, size_t threads_num) {
  self->parts = (part_int_t*)malloc(sizeof(part_int_t) * threads_num);
  self->threads = (pthread_t*)malloc(sizeof(pthread_t) * threads_num);
  self->threads_num = threads_num;
  self->step = powl(10.0l, -4l);
  self->atomic = 0;
  for (int i = 0; i < threads_num; ++i) {
    self->parts[i].step = self->step;
    self->parts[i].ptr = &self->atomic;
    self->parts[i].old = 0;
    self->parts[i].need_to_exit = 0;
    self->parts[i].is_run = 1;
    pthread_create(self->threads + i, NULL, integrate, self->parts + i);
  }
  _Atomic(uint32_t) is_run = 0;
  for (int i = 0; i < threads_num; ++i) {
    while (true) {
      if (atomic_compare_exchange_weak(&self->parts[i].is_run, &is_run, 0)) {
        break;
      }
      is_run = 0;
    }
  }
  return 0;
}

int par_integrator_start_calc(par_integrator_t* self, func_t* func,
                              field_t left_bound, field_t right_bound) {
  double threads_num = self->threads_num;
  double piece = (right_bound - left_bound) / threads_num;
  for (int i = 0; i < threads_num; ++i) {
    self->parts[i].left_bound = left_bound + piece * (double)i;
    self->parts[i].right_bound = left_bound + piece * ((double)i + 1.0l);
    self->parts[i].func = func;
    self->parts[i].is_run = 1;
  }

  ++self->atomic;
  atomic_notify_all(&self->atomic);

  return 0;
}

int par_integrator_get_result(par_integrator_t* self, field_t* result) {
  *result = 0;
  _Atomic(uint32_t) is_run = 0;
  for (int i = 0; i < self->threads_num; ++i) {
    while (true) {
      if (atomic_compare_exchange_weak(&self->parts[i].is_run, &is_run, 0)) {
        break;
      }
      is_run = 0;
    }
  }
  for (int i = 0; i < self->threads_num; ++i) {
    *result += self->parts[i].result;
  }
  return 0;
}

int par_integrator_destroy(par_integrator_t* self) {
  for (int i = 0; i < self->threads_num; ++i) {
    self->parts[i].is_run = 1;
    self->parts[i].need_to_exit = 1;
  }

  ++self->atomic;
  atomic_notify_all(&self->atomic);
  for (int i = 0; i < self->threads_num; ++i) {
    pthread_join(self->threads[i], NULL);
  }

  free(self->threads);
  free(self->parts);
  return 0;
}