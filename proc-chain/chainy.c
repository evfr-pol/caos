#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <sys/wait.h>

enum {
    MAX_ARGS_COUNT = 256,
    MAX_CHAIN_LINKS_COUNT = 256,
    MAX_COMMAND_SIZE = 256
};

typedef struct {
    char command[MAX_COMMAND_SIZE];
    uint64_t argc;
    char* argv[MAX_ARGS_COUNT];
} chain_link_t;

typedef struct {
    uint64_t chain_links_count;
    chain_link_t chain_links[MAX_CHAIN_LINKS_COUNT];
} chain_t;

void create_chain(char* command, chain_t* chain) {
    uint64_t i = 0;
    uint64_t cur_chain_link = 0;
    chain->chain_links[0].argc = 0;
    while (i < strlen(command)) {
        if (command[i] == '|') {
            ++cur_chain_link;
            ++i;
            chain->chain_links[cur_chain_link].argc = 0;
            continue;
        }
        if (command[i] == ' ') {
            ++i;
            continue;
        }
        if (chain->chain_links[cur_chain_link].argc != 0) {
            char str[MAX_COMMAND_SIZE];
            memset(str, '\0', MAX_COMMAND_SIZE);
            char helper[2];
            memset(helper, '\0', 2);
            while (i < strlen(command) && command[i] != ' ') {
                sprintf(helper, "%c", command[i]);
                strcat(str, helper);
                ++i;
            }
            chain->chain_links[cur_chain_link].argv[chain->chain_links[cur_chain_link].argc] = strdup(str);
            ++chain->chain_links[cur_chain_link].argc;
            continue;
        }
        char str[MAX_COMMAND_SIZE];
        memset(str, '\0', MAX_COMMAND_SIZE);
        char helper[2];
        memset(helper, '\0', 2);
        while (i < strlen(command) && command[i] != ' ') {
            sprintf(helper, "%c", command[i]);
            strcat(str, helper);
            ++i;
        }
        chain->chain_links[cur_chain_link].argv[chain->chain_links[cur_chain_link].argc] = strdup(str);
        sprintf(chain->chain_links[cur_chain_link].command, "%s", str);
        ++chain->chain_links[cur_chain_link].argc;
    }
    chain->chain_links_count = cur_chain_link;
}

void run_chain(chain_t* chain) {
    int pipe_fd[chain->chain_links_count + 1][2];

    if (pipe(pipe_fd[0]) == -1) {
        perror("Pipe in begin");
        exit(2);
    }
    pid_t beg_pid = fork();
    if (beg_pid == -1) {
        perror("Fork in begin");
        exit(2);
    }
    if (beg_pid == 0) {
        if (close(pipe_fd[0][0]) == -1) {
            perror("Close pipe in begin");
            exit(2);
        }
        if (close(1) == -1) {
            perror("Close in begin");
            exit(2);
        }
        if (dup2(pipe_fd[0][1], 1) == -1) {
            perror("Dup2 in child");
            exit(2);
        }
        int err = execvp(chain->chain_links[0].command, chain->chain_links[0].argv);
        if (err == -1) {
            perror("Execvp in begin");
            exit(2);
        }
    } else {
        if (close(pipe_fd[0][1]) == -1) {
            perror("Close pipe in parent");
            exit(2);
        }
        if (close(0) == -1) {
            perror("Close");
            exit(2);
        }
        if (dup2(pipe_fd[0][0], 0) == -1) {
            perror("Dup2 in parent");
            exit(2);
        }
    }
    for (int i = 1; i < chain->chain_links_count; ++i) {
        if (pipe(pipe_fd[i]) != 0) {
            perror("Pipe");
            exit(3);
        }
        pid_t ls_pid = fork();
        if (ls_pid == -1) {
            perror("Fork");
            exit(3);
        }
        if (ls_pid == 0) {
            if (close(pipe_fd[i][0]) == -1) {
                perror("Close pipe");
                exit(3);
            }
            if (close(1) == -1) {
                perror("Close");
                exit(4);
            }
            if (dup2(pipe_fd[i][1], 1) == -1) {
                perror("Dup2 in child");
                exit(5);
            }
            if (close(0) == -1) {
                perror("Close");
                exit(4);
            }
            if (dup2(pipe_fd[i - 1][0], 0) == -1) {
                perror("Dup2 in child");
                exit(5);
            }
            int err = execvp(chain->chain_links[i].command, chain->chain_links[i].argv);
            if (err == -1) {
                perror("Execvp");
                exit(6);
            }
        } else {
            if (close(pipe_fd[i - 1][0]) == -1) {
                perror("Close");
                exit(3);
            }
            if (close(pipe_fd[i][1]) != 0) {
                perror("Close pipe");
                exit(7);
            }
            close(0);
            if (dup2(pipe_fd[i][0], 0) == -1) {
                perror("Dup2 in parent");
                exit(8);
            }
        }
    }
    

    pid_t last_func = fork();
    if (last_func == -1) {
        perror("Fork in last func");
        exit(9);
    }
    if (last_func == 0) {
        if (close(0) == -1) {
            perror("Close in last func");
            exit(10);
        }
        if (dup2(pipe_fd[chain->chain_links_count - 1][0], 0) == -1) {
            perror("Dup2 in last func");
            exit(11);
        }
        int err = execvp(chain->chain_links[chain->chain_links_count].command, chain->chain_links[chain->chain_links_count].argv);
        if (err  == -1) {
            perror("Execvp in last func");
            exit(12);
        }
    } else {
        if (close(0) == -1) {
            perror("Close in the end");
            exit(13);
        }
        if (close(1) == -1) {
            perror("Close in the end");
            exit(14);
        }
        if (close(pipe_fd[chain->chain_links_count - 1][0]) == -1) {
            perror("Close pipe in the end");
            exit(15);
        }
        if (waitpid(last_func, NULL, 0) == -1) {
            perror("Wait");
            exit(16);
        }
    }
}

void run_one(chain_t* chain) {
    int err = execvp(chain->chain_links[chain->chain_links_count].command, chain->chain_links[chain->chain_links_count].argv);
    if (err == -1) {
        perror("Execvp in run_one");
        exit(17);
    }
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        perror("Wrong argc");
        exit(1);
    }
    chain_t chain;
    create_chain(argv[1], &chain);
    if (chain.chain_links_count != 0) {
        run_chain(&chain);
    } else {
        run_one(&chain);
    }
    return 0;
}
