#include <dirent.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int rm_dir(char* path) {
  DIR* direct = opendir(path);
  if (direct == NULL) {
    return -1;
  }
  struct dirent* cur;
  while ((cur = readdir(direct)) != NULL) {
    char new_cur[PATH_MAX];
    struct stat new;
    if ((strcmp(cur->d_name, ".") == 0) || (strcmp(cur->d_name, "..") == 0)) {
      continue;
    }
    snprintf(new_cur, PATH_MAX, "%s/%s", path, cur->d_name);
    if (lstat(new_cur, &new) == -1) {
      return -1;
    }
    if (S_ISDIR(new.st_mode)) {
      rm_dir(new_cur);
    } else {
      if (unlink(new_cur) == -1) {
        return -1;
      }
    }
  }
  if (closedir(direct) == -1) {
    return -1;
  }
  if (rmdir(path) == -1) {
    return -1;
  }
  return 0;
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    return -1;
  }
  bool is_there_r = true;
  bool encounter_dir_with_no_r = false;
  int count = getopt(argc, argv, "r");
  if (count == -1) {
    is_there_r = false;
  }
  getopt(argc, argv, "r");
  struct stat thing;
  for (int i = optind; i < argc; ++i) {
    if (lstat(argv[i], &thing) == -1) {
      return -1;
    }
    if (S_ISDIR(thing.st_mode)) {
      if (is_there_r) {
        if (rm_dir(argv[i]) == -1) {
          perror("Cannot remove directory");
          return -1;
        }
      } else {
        encounter_dir_with_no_r = true;
      }
    } else {
      if (unlink(argv[i]) == -1) {
        perror("Cannot remove file");
        return -2;
      }
    }
  }
  if (encounter_dir_with_no_r) {
    perror("Try remove directory without -r");
    return -3;
  }
  return 0;
}
