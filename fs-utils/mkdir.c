#include <dirent.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

bool check_if_need_parent(char* path) {
  DIR* exist = opendir(path);
  if (exist != NULL) {
    closedir(exist);
    return false;
  }
  char* prev_slash = strrchr(path, '/');
  if (prev_slash == NULL) {
    return false;
  }
  char new_path[PATH_MAX];
  for (int i = 0; i <= prev_slash - path - 1; ++i) {
    new_path[i] = path[i];
  }
  new_path[prev_slash - path] = '\0';
  exist = opendir(new_path);
  if (exist != NULL) {
    closedir(exist);
    return false;
  }
  return true;
}

int create_dir(char* path, mode_t mode, bool is_first) {
  DIR* exist = opendir(path);
  if (exist != NULL) {
    closedir(exist);
    return 0;
  }
  char* prev_slash = strrchr(path, '/');
  if (prev_slash == NULL) {
    if (is_first) {
      if (mkdir(path, mode) == -1) {
        return -1;
      }
    } else {
      if (mkdir(path, 0777) == -1) {
        return -1;
      }
    }

  } else {
    char new_path[PATH_MAX];
    for (int i = 0; i <= prev_slash - path - 1; ++i) {
      new_path[i] = path[i];
    }
    new_path[prev_slash - path] = '\0';
    if (create_dir(new_path, mode, false) == -1) {
      return -1;
    }
    if (is_first) {
      if (mkdir(path, mode) == -1) {
        return -1;
      }
    } else {
      if (mkdir(path, 0777) == -1) {
        return -1;
      }
    }
  }
  return 0;
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    return -1;
  }
  static struct option op[] = {{"mode", required_argument, 0, 'm'},
                               {0, 0, 0, 0}};
  int count;
  bool is_there_p = false;
  bool is_error_with_p = false;
  mode_t mode = 0777;
  while ((count = getopt_long(argc, argv, "pm:", op, NULL)) != -1) {
    if (count == 'p') {
      is_there_p = true;
    } else {
      if (count == 'm') {
        if (optarg[0] == '=') {
          mode = strtol(optarg + 1, NULL, 8);
        } else {
          mode = strtol(optarg, NULL, 8);
        }
      }
    }
  }
  for (int i = optind; i < argc; ++i) {
    if (check_if_need_parent(argv[i])) {
      if (is_there_p) {
        if (create_dir(argv[i], mode, true) == -1) {
          return -1;
        }
      } else {
        is_error_with_p = true;
      }
    } else {
      if (create_dir(argv[i], mode, true) == -1) {
        return -1;
      }
    }
  }
  if (is_error_with_p) {
    return -1;
  }
  return 0;
}
