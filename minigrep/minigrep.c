#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <pcre.h>
#include <regex.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

void grep_file(const char* path, const regex_t* preg) {
  struct stat st;
  regmatch_t regmatch[2];
  if (stat(path, &st) == -1) {
    perror("Stat");
    exit(1);
  }
  size_t size = st.st_size;
  char buf[1000000];
  memset(buf, '\0', 1000000);
  int fd = open(path, O_RDONLY);
  if (fd == -1) {
    perror("Open");
    exit(2);
  }
  char* ptr =
      (char*)(mmap(NULL, size, PROT_READ, MAP_PRIVATE | MAP_FILE, fd, 0));
  if (ptr == MAP_FAILED) {
    perror("Mmap");
    exit(3);
  }
  int cur_ind = 0;
  int cur_line = 0;
  for (int i = 0; i < size; ++i) {
    if (*(ptr + i) != '\n') {
      buf[cur_ind] = *(ptr + i);
      ++cur_ind;
    } else {
      if (regexec(preg, buf, 1, regmatch, 0) == 0) {
        char helper[10000];
        memset(helper, '\0', 10000);
        sprintf(helper, "%s:%d: ", path, cur_line + 1);
        strcat(helper, buf);
        printf("%s\n", helper);
      }
      memset(buf, '\0', 1000000);
      cur_ind = 0;
      ++cur_line;
    }
  }
  if (regexec(preg, buf, 1, regmatch, 0) == 0) {
    printf("%s\n", buf);
  }
  if (munmap(ptr, size) == -1) {
    perror("Munmap");
    exit(5);
  }
  if (close(fd) == -1) {
    perror("Close");
    exit(4);
  }
}

int grep_dir(const char* path, const regex_t* preg) {
  DIR* direct = opendir(path);
  if (direct == NULL) {
    return -1;
  }
  struct dirent* cur;
  while ((cur = readdir(direct)) != NULL) {
    char new_cur[PATH_MAX];
    struct stat new;
    if ((strcmp(cur->d_name, ".") == 0) || (strcmp(cur->d_name, "..") == 0)) {
      continue;
    }
    snprintf(new_cur, PATH_MAX, "%s/%s", path, cur->d_name);
    if (lstat(new_cur, &new) == -1) {
      return -1;
    }
    if (S_ISDIR(new.st_mode)) {
      grep_dir(new_cur, preg);
    } else {
      grep_file(new_cur, preg);
    }
  }
  if (closedir(direct) == -1) {
    return -1;
  }
  return 0;
}

int main(int argc, char* argv[]) {
  const char* regex = argv[1];
  const char* dir = argv[2];
  const char* errbuf;
  int offset;

  pcre* pattern = pcre_compile(regex, PCRE_UTF8 | PCRE_NO_UTF8_CHECK, &errbuf,
                               &offset, NULL);

  if (pattern == NULL) {
    fputs(errbuf, stderr);
    free((void*)errbuf);
    free(pattern);
    return 1;
  }
  free((void*)errbuf);
  free(pattern);

  regex_t preg;
  regcomp(&preg, regex, 0);

  grep_dir(dir, &preg);
  regfree(&preg);

  return 0;
}
