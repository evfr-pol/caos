#include "bloom_filter.h"

#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

uint64_t power(uint64_t a, uint64_t b) {
  uint64_t power_st = a;
  if (b == 0) {
    return 1;
  }
  for (uint64_t i = 0; i < b - 1; ++i) {
    a *= power_st;
  }
  return a;
}

uint64_t calc_hash(const char* str, uint64_t modulus, uint64_t seed) {
  uint64_t result = 0;
  for (uint64_t i = 0; str[i] != '\0'; ++i) {
    printf("1\n");
    result += (str[i] * power(seed, i)) % modulus;
    result %= modulus;
  }
  return result;
}

void bloom_init(struct BloomFilter* bloom_filter, uint64_t set_size,
                hash_fn_t hash_fn, uint64_t hash_fn_count) {
  bloom_filter->set = (uint64_t*)calloc(set_size, sizeof(uint64_t));
  bloom_filter->set_size = set_size;
  bloom_filter->hash_fn = hash_fn;
  bloom_filter->hash_fn_count = hash_fn_count;
}

void bloom_destroy(struct BloomFilter* bloom_filter) {
  if (bloom_filter) {
    if (bloom_filter->set) {
      free(bloom_filter->set);
      bloom_filter->set = NULL;
    }
  }
}

void bloom_insert(struct BloomFilter* bloom_filter, Key key) {
  uint64_t result;
  for (uint64_t i = 0; i < bloom_filter->hash_fn_count; ++i) {
    result = (bloom_filter->hash_fn)(key, bloom_filter->set_size, 2ull + i);
    bloom_filter->set[result / 64ull] |= (1ull << (result % 64));
  }
}

bool bloom_check(struct BloomFilter* bloom_filter, Key key) {
  bool answer = true;
  uint64_t result;
  for (uint64_t i = 0; i < bloom_filter->hash_fn_count; ++i) {
    result = (bloom_filter->hash_fn)(key, bloom_filter->set_size, 2ull + i);
    if (!(bloom_filter->set[result / 64ull] & (1ull << (result % 64ull)))) {
      answer = false;
      break;
    }
  }
  return answer;
}