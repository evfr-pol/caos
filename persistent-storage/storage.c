#include "storage.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <inttypes.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

void storage_init(storage_t* storage, const char* root_path) {
  storage->root_path = root_path;
}

int rm_dir(char* path, const char* motherpath) {
  DIR* direct = opendir(path);
  if (direct == NULL) {
    return -1;
  }
  struct dirent* cur;
  while ((cur = readdir(direct)) != NULL) {
    char new_cur[PATH_MAX];
    struct stat new;
    if ((strcmp(cur->d_name, ".") == 0) || (strcmp(cur->d_name, "..") == 0)) {
      continue;
    }
    snprintf(new_cur, PATH_MAX, "%s/%s", path, cur->d_name);
    if (lstat(new_cur, &new) == -1) {
      return -1;
    }
    if (S_ISDIR(new.st_mode)) {
      rm_dir(new_cur, motherpath);
    } else {
      if (unlink(new_cur) == -1) {
        return -1;
      }
    }
  }
  if (closedir(direct) == -1) {
    return -1;
  }
  if (path != motherpath) {
    if (rmdir(path) == -1) {
      return -1;
    }
  }
  return 0;
}

void storage_destroy(storage_t* storage) {
  // rm_dir(storage->root_path, storage->root_path);
}

void make_reg_path(const char* mother_path, const char* child_path,
                   char* path) {
  int mother_size = strlen(mother_path);
  for (int i = 0; i < mother_size; ++i) {
    path[i] = mother_path[i];
  }
  path[mother_size] = '/';
  int count = 0;
  int offset = 0;
  for (int i = 0; i < strlen(child_path); ++i) {
    if (count == SUBDIR_NAME_SIZE) {
      path[mother_size + 1 + offset + i] = '/';
      ++offset;
      count = 0;
    }
    path[mother_size + 1 + offset + i] = child_path[i];
    ++count;
  }
  if (strlen(child_path) % SUBDIR_NAME_SIZE == 0) {
    path[mother_size + 1 + offset + strlen(child_path)] = '/';
    path[mother_size + 2 + offset + strlen(child_path)] = '@';
    path[mother_size + 3 + offset + strlen(child_path)] = '\0';
  } else {
    path[mother_size + 1 + offset + strlen(child_path)] = '\0';
  }
}

int create_dir(char* path) {
  DIR* exist = opendir(path);
  if (exist != NULL) {
    closedir(exist);
    return 0;
  }
  char* prev_slash = strrchr(path, '/');
  if (prev_slash == NULL) {
    if (mkdir(path, 0777) == -1) {
      return -1;
    }
  } else {
    char new_path[PATH_MAX];
    for (int i = 0; i <= prev_slash - path - 1; ++i) {
      new_path[i] = path[i];
    }
    new_path[prev_slash - path] = '\0';
    if (create_dir(new_path) == -1) {
      return -1;
    }
    if (mkdir(path, 0777) == -1) {
      return -1;
    }
  }
  return 0;
}

void create_path_to_directory(const char* old_path, char* new_path) {
  char* pos_of_last_slash = strrchr(old_path, '/');
  if (pos_of_last_slash == NULL) {
    for (int i = 0; i <= strlen(old_path); ++i) {
      new_path[i] = old_path[i];
    }
  } else {
    int i = 0;
    for (; (old_path + i) != pos_of_last_slash; ++i) {
      new_path[i] = old_path[i];
    }
    new_path[i] = '\0';
  }
}

void create_version(version_t vers, uint8_t* ans) {
  ans[0] = (vers >> 56ull);
  for (uint64_t i = 1ull; i < 8ull; ++i) {
    ans[i] = (vers >> (56ull - 8ull * i)) & ((1ull << 8ull) - 1ull);
  }
}

version_t decode(uint8_t* to_decode) {
  version_t vers = to_decode[0];
  for (int i = 1ull; i < 8ull; ++i) {
    vers <<= 8ull;
    vers += to_decode[i];
  }
  return vers;
}

version_t storage_set(storage_t* storage, storage_key_t key,
                      storage_value_t value) {
  char path[PATH_MAX];
  char path_to_dir[PATH_MAX];
  make_reg_path(storage->root_path, key, path);
  create_path_to_directory(path, path_to_dir);
  struct stat status;
  uint64_t version_to_answer;
  if (lstat(path, &status) == -1) {
    if (errno == ENOENT) {
      create_dir(path_to_dir);
      int fd = creat(path, 0777);
      uint8_t version[8];
      uint64_t num_of_version = 1ull;
      create_version(num_of_version, version);
      uint64_t num_length = strlen(value) + 1;
      uint8_t length[8];
      create_version(num_length, length);
      write(fd, version, 8);
      write(fd, length, 8);
      write(fd, value, strlen(value) + 1);
      write(fd, version, 8);
      write(fd, length, 8);
      close(fd);
      return 1;
    }
  } else {
    int fd = open(path, O_RDWR);
    lseek(fd, -16, SEEK_END);
    uint8_t version[8];
    read(fd, version, 8);
    uint64_t num_of_version = decode(version);
    ++num_of_version;
    version_to_answer = num_of_version;
    create_version(num_of_version, version);
    uint64_t num_length = strlen(value) + 1;
    uint8_t length[8];
    create_version(num_length, length);
    lseek(fd, 0, SEEK_END);
    write(fd, version, 8);
    write(fd, length, 8);
    write(fd, value, strlen(value) + 1);
    write(fd, version, 8);
    write(fd, length, 8);
    close(fd);
    return num_of_version;
  }
  return version_to_answer;
}

version_t storage_get(storage_t* storage, storage_key_t key,
                      returned_value_t returned_value) {
  char path[PATH_MAX];
  make_reg_path(storage->root_path, key, path);
  struct stat status;
  if (lstat(path, &status) == -1) {
    if (errno == ENOENT) {
      return 0;
    }
  }
  int fd = open(path, O_RDONLY);
  uint8_t length[8];
  uint8_t version[8];
  lseek(fd, -16, SEEK_END);
  read(fd, version, 8);
  read(fd, length, 8);
  uint64_t version_to_answer = decode(version);
  uint64_t num_length = decode(length);
  lseek(fd, -16 - num_length, SEEK_END);
  read(fd, returned_value, num_length);
  close(fd);
  return version_to_answer;
}

version_t storage_get_by_version(storage_t* storage, storage_key_t key,
                                 version_t version,
                                 returned_value_t returned_value) {
  char path[PATH_MAX];
  make_reg_path(storage->root_path, key, path);
  struct stat status;
  if (lstat(path, &status) == -1) {
    if (errno == ENOENT) {
      return 0;
    }
  }
  int fd = open(path, O_RDONLY);
  uint8_t length[8];
  uint8_t version_cur[8];
  read(fd, version_cur, 8);
  read(fd, length, 8);
  uint64_t num_vers_cur = decode(version_cur);
  uint64_t num_length = decode(length);
  while (num_vers_cur != version) {
    lseek(fd, num_length + 16, SEEK_CUR);
    read(fd, version_cur, 8);
    read(fd, length, 8);
    num_vers_cur = decode(version_cur);
    num_length = decode(length);
  }
  read(fd, returned_value, num_length);
  lseek(fd, -16, SEEK_END);
  read(fd, version_cur, 8);
  num_vers_cur = decode(version_cur);
  close(fd);
  return version;
}