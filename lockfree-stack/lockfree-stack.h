#pragma once

#include <pthread.h>
#include <stdatomic.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct node {
  struct node* next;
  uintptr_t value;
} node_t;

typedef struct lfstack {
  node_t* top;
} lfstack_t;

int lfstack_init(lfstack_t* stack) {
  if (stack == NULL) {
    return -1;
  }
  stack->top = NULL;
  return 0;  // success
}

int lfstack_push(lfstack_t* stack, uintptr_t value) {
  if (stack == NULL) {
    return -1;
  }
  node_t* node_to_ins = (node_t*)malloc(sizeof(node_t));
  if (node_to_ins == NULL) {
    return -1;
  }
  node_to_ins->value = value;
  node_t* old_top = atomic_load(&(stack->top));
  ;
  while (1) {
    node_to_ins->next = old_top;
    if (atomic_compare_exchange_weak(&(stack->top), &old_top, node_to_ins)) {
      break;
    }
  }
  return 0;  // success
}

int lfstack_pop(lfstack_t* stack, uintptr_t* value) {
  if (stack == NULL || value == NULL) {
    return -1;
  }
  node_t* top = atomic_load(&(stack->top));
  if (top == NULL) {
    *value = 0;
    return 0;
  }
  while (top != NULL) {
    if (atomic_compare_exchange_weak(&(stack->top), &top, top->next)) {
      *value = top->value;
      free(top);
      return 0;
    }
  }
  *value = 0;
  return 0;  // success
}

int lfstack_destroy(lfstack_t* stack) {
  if (stack == NULL) {
    return -1;
  }
  node_t* top = stack->top;
  node_t* next_top;
  while (top != NULL) {
    next_top = top->next;
    free(top);
    top = next_top;
  }
  return 0;  // success
}
