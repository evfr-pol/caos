    .intel_syntax noprefix

    .text
    .global dot_product

dot_product:
    push rbp
    mov rbp, rsp

    pxor xmm0, xmm0
    pxor xmm1, xmm1
    pxor xmm2, xmm2
    cmp rdi, 4
    jb bite

loop:
    movups xmm1, [rsi]
    movups xmm2, [rdx]
    add rsi, 16
    add rdx, 16
    mulps xmm1, xmm2
    addps xmm0, xmm1
    sub rdi, 4
    cmp rdi, 4
    jae loop
    
    cmp rdi, 0
    jz end
    pxor xmm1, xmm1
    pxor xmm2, xmm2

bite:
    movss xmm1, [rsi]
    movss xmm2, [rdx]
    add rsi, 4
    add rdx, 4
    mulss xmm1, xmm2
    addss xmm0, xmm1
    sub rdi, 1
    cmp rdi, 0
    ja bite

end:
    haddps xmm0, xmm0
    haddps xmm0, xmm0
    mov rsp, rbp
    pop rbp
    ret
