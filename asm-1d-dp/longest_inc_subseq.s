  .text
  .global longest_inc_subseq

longest_inc_subseq:
  cmp x2, 0
  bne ne
  mov x0, 0
  ret

ne:
  mov x3, 0
  mov x15, 1
loop:
  cmp x3, x2
  beq loopend
  str  x15, [x1, x3, lsl 3]
  mov x4, -1
  innerloop:
    add x4, x4, 1
    cmp x4, x3
    beq innerloopend
    ldr x5, [x0, x3, lsl 3]
    ldr x6, [x0, x4, lsl 3]
    cmp x6, x5
    bge innerloop
    ldr x9, [x1, x3, lsl 3]
    ldr x10, [x1, x4, lsl 3]
    add x10, x10, 1
    cmp x9, x10
    bhi innerloop
    str x10, [x1, x3, lsl 3]
    b innerloop
  innerloopend:
  add x3, x3, 1
  b loop

loopend:
  ldr x11, [x1]

  mov x3, -1
newloop:
  add x3, x3, 1
  cmp x3, x2
  beq end
  ldr x9, [x1, x3, lsl 3]
  cmp x11, x9
  bhi newloop
  mov x11, x9
  b newloop
end:
  mov x0, x11
  ret
