  .intel_syntax noprefix

  .text
  .global my_memcpy

my_memcpy:
  push rbp
  mov rbp, rsp

  mov r8, rdi
  cmp edx, 0
  jz end
  cmp edx, 8
  jb bite

loop:
  mov rcx, [rsi]
  mov [rdi], rcx
  add rdi, 8
  add rsi, 8
  sub edx, 8
  cmp edx, 8
  ja loop

  cmp edx, 0
  jz end

bite:
  mov al, byte ptr [rsi]
  mov byte ptr [rdi], al
  add rdi, 1
  add rsi, 1
  sub edx, 1
  cmp edx, 0
  jnz bite

end:
  mov rax, r8

  mov rsp, rbp
  pop rbp
  ret

