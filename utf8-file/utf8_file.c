#include "utf8_file.h"

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int dop_bytes_in_symbol(uint8_t* buf) {
  uint8_t first = buf[0];
  if ((first >> 7) == 0) {
    return 0;
  }
  if ((first >> 5) == 6) {
    return 1;
  }
  if ((first >> 4) == 14) {
    return 2;
  }
  if ((first >> 3) == 30) {
    return 3;
  }
  if ((first >> 2) == 62) {
    return 4;
  }
  if ((first >> 1) == 126) {
    return 5;
  }
  return -1;
}

int check_dop_byte(uint8_t* buf) {
  if ((*buf) >> 6 == 2) {
    return 1;
  }
  return 0;
}

uint32_t first_byte_to_uint(uint8_t* buf) {
  uint8_t first = buf[0];
  if ((first >> 7) == 0) {
    return first;
  }
  if ((first >> 5) == 6) {
    return (first % 32);
  }
  if ((first >> 4) == 14) {
    return (first % 16);
  }
  if ((first >> 3) == 30) {
    return (first % 8);
  }
  if ((first >> 2) == 62) {
    return (first % 4);
  }
  if ((first >> 1) == 126) {
    return (first % 2);
  }
  return 0;
}

uint32_t dop_byte_to_uint(uint8_t* buf) { return ((*buf) % 64); }

uint32_t bytes_to_symb(uint8_t* buf, int count_bytes, int* error) {
  uint32_t result = 0;
  for (int i = 1; i <= count_bytes; ++i) {
    if (check_dop_byte(buf + i) != 1) {
      *error = -1;
      return 0;
    }
  }
  result += first_byte_to_uint(buf);
  for (int i = 1; i <= count_bytes; ++i) {
    result <<= 6;
    result += dop_byte_to_uint(buf + i);
  }
  return result;
}

int utf8_read(utf8_file_t* f, uint32_t* res, size_t count) {
  int fd = f->fd;
  size_t prev_count = count;
  uint8_t* buf = (uint8_t*)calloc(4, sizeof(uint8_t));
  int amount = 0;
  int count_bytes = 0;
  while (count > 0) {
    amount = read(fd, buf, 1);
    if (amount == 0) {
      break;
    }
    if (amount == -1) {
      free(buf);
      return -1;
    }
    count_bytes = dop_bytes_in_symbol(buf);
    if (count_bytes == -1) {
      free(buf);
      errno = EILSEQ;
      return -1;
    }
    if (count_bytes > 0) {
      amount = read(fd, buf + 1, count_bytes);
      if (amount < count_bytes && amount >= 0) {
        free(buf);
        errno = EILSEQ;
        return -1;
      }
      if (amount == -1) {
        return -1;
      }
    }
    int error = 0;
    uint32_t result = bytes_to_symb(buf, count_bytes, &error);
    if (error == -1) {
      free(buf);
      errno = EILSEQ;
      return -1;
    }
    (*res) = result;
    ++res;
    --count;
  }
  free(buf);
  return (prev_count - count);
}

int how_many_bite(uint32_t str) {
  if ((str % (1ul << 7ul)) == str) {
    return 0;
  }
  if ((str % (1ul << 11ul)) == str) {
    return 1;
  }
  if ((str % (1ul << 16ul)) == str) {
    return 2;
  }
  if ((str % (1ul << 21ul)) == str) {
    return 3;
  }
  return -1;
}

void str_to_byte(uint32_t str, uint8_t* buf, int* amount) {
  int count = how_many_bite(str);
  if (count == -1) {
    *amount = -1;
    return;
  }
  *amount = count;
  if (count == 0) {
    buf[0] = str;
    return;
  }
  if (count == 1) {
    buf[0] = (6 << 5);
    buf[0] += (str >> 6);
    buf[1] = (2 << 6);
    buf[1] += str % 64;
    return;
  }
  if (count == 2) {
    buf[0] = (14 << 4);
    buf[0] += (str >> 12);
    buf[1] = (2 << 6);
    buf[1] += (str >> 6) % 64;
    buf[2] = (2 << 6);
    buf[2] += str % 64;
    return;
  }
  if (count == 3) {
    buf[0] = (30 << 3);
    buf[0] += (str >> 21);
    buf[1] = (2 << 6);
    buf[1] += (str >> 12) % 64;
    buf[2] = (2 << 6);
    buf[2] += (str >> 6) % 64;
    buf[3] = (2 << 6);
    buf[3] += str % 64;
    return;
  }
}

int utf8_write(utf8_file_t* f, const uint32_t* str, size_t count) {
  int fd = f->fd;
  size_t prev_count = count;
  uint8_t* buf = (uint8_t*)calloc(4, sizeof(uint8_t));
  int amount = 0;
  int count_symb = 0;
  while (count > 0) {
    str_to_byte(*(str + prev_count - count), buf, &amount);
    if (amount == -1) {
      free(buf);
      errno = EILSEQ;
      return -1;
    }
    count_symb = write(fd, buf, amount + 1);
    if (count_symb == -1) {
      free(buf);
      return -1;
    }
    if (count_symb >= 0 && count_symb < amount + 1) {
      free(buf);
      errno = EILSEQ;
      return -1;
    }
    --count;
  }
  free(buf);
  return (prev_count - count);
}

utf8_file_t* utf8_fromfd(int fd) {
  utf8_file_t* fl = (utf8_file_t*)calloc(1, sizeof(utf8_file_t));
  fl->fd = fd;
  return fl;
}
